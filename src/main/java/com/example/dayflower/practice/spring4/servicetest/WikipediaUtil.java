package com.example.dayflower.practice.spring4.servicetest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Component;

@Component
public class WikipediaUtil {
	private WikipediaService wikipediaService;

	@Required
	public void setWikipediaService(WikipediaService wikipediaService) {
		this.wikipediaService = wikipediaService;
	}

	private static final Pattern CATEGORY_PATTERN = Pattern.compile("\\[\\[ Category: (.+?) (?: \\| .+ )? \\]\\]", Pattern.COMMENTS);

	public List<String> getPageCategories(String title) {
		WikipediaServiceImpl.PageModel page = wikipediaService.getPage(title);
		if (!page.isFound()) {
			return Collections.emptyList();
		}

		Matcher matcher = CATEGORY_PATTERN.matcher(page.getContent());
		List<String> categories = new ArrayList<>();
		while (matcher.find()) {
			categories.add(matcher.group(1));
		}

		return categories;
	}
}
