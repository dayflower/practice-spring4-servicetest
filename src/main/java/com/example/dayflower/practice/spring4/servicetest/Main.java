package com.example.dayflower.practice.spring4.servicetest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@Slf4j
public class Main {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		WikipediaUtil wikipediaUtil = context.getBean(WikipediaUtil.class);
		log.info("categories: {}", wikipediaUtil.getPageCategories("寿司"));
	}

}
