package com.example.dayflower.practice.spring4.servicetest;

import java.util.Arrays;
import java.util.List;
import lombok.Setter;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class WikipediaUtilTest {
	@Autowired
	@Setter
	WikipediaUtil wikipediaUtil;

	@Test
	public void testGetPageCategoriesForExist() {
		List<String> categories = wikipediaUtil.getPageCategories("ハオプテルス2");
		assertThat(categories, is(Arrays.<String>asList("白亜紀の翼竜", "翼指竜亜目")));
	}
}
