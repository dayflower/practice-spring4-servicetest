package com.example.dayflower.practice.spring4.servicetest;

import com.google.common.collect.ImmutableMap;
import java.util.Map;

public class WikipediaServiceStub implements WikipediaService {

	private static final String CONTENT_HAOPTERUS = "'''ハオプテルス'''（'''''Haopterus'''''）は、[[中生代]][[白亜紀]]前期に生息していた[[翼竜]]の一種。 {{DEFAULTSORT:はおふてるす}} [[Category:白亜紀の翼竜]] [[Category:翼指竜亜目]] {{paleo-stub}}";
	private static final String CONTENT_ISLANDS_OF_MALAWI = "[[マラウイ]]の島の一覧 * [[チズムル島]] * [[リコマ島]] * [[チシ島]] {{アフリカの題材|島の一覧}} [[Category:各国の島の一覧|まらうい]] [[Category:マラウイの地形|しま]] [[Category:マラウイ関連一覧|しま]]";
	private static final Map<String, String> PAGE_CONTENT_MAP = ImmutableMap.of("ハオプテルス2", CONTENT_HAOPTERUS, "マラウイの島の一覧", CONTENT_ISLANDS_OF_MALAWI);

	@Override
	public PageModel getPage(String title) {
		if (PAGE_CONTENT_MAP.containsKey(title)) {
			return PageModel.builder().found(true).pageId(1738111).title(title).content(PAGE_CONTENT_MAP.get(title)).build();
		} else {
			return PageModel.builder().found(false).build();
		}
	}

}
