package com.example.dayflower.practice.spring4.servicetest;

import java.util.Arrays;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@Slf4j
public class WikipediaServiceImpl implements WikipediaService {

	@Autowired
	private RestTemplate restTemplate;

	@Override
	public PageModel getPage(String title) {
		UriComponents uri = UriComponentsBuilder.fromUriString("http://ja.wikipedia.org/w/api.php")
			.queryParam("action", "query")
			.queryParam("prop", "revisions")
			.queryParam("rvprop", "content")
			.query("redirects")
			.query("continue")
			.queryParam("format", "json")
			.queryParam("titles", title)
			.build();

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<>(headers);

//		ResponseEntity<QueryPageResult> res = restTemplate.getForEntity(uri.toString(), QueryPageResult.class);
		ResponseEntity<QueryPageResult> res = restTemplate.exchange(uri.toString(), HttpMethod.GET, entity, QueryPageResult.class);
		if (!res.getStatusCode().is2xxSuccessful()) {
			log.error("failed: {}", res.getStatusCode().toString());
			return PageModel.builder().found(false).build();
		}

		return res.getBody().getFirstPage();
	}

}
