package com.example.dayflower.practice.spring4.servicetest;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Data;

public interface WikipediaService {
	public PageModel getPage(String title);

	@Data
	@Builder
	public static class PageModel {
		private boolean found;
		private int pageId;
		private String title;
		private String content;
	}

	@Data
	public static class QueryPageResult {
		private QueryContent query;

		public PageModel getFirstPage() {
			PageModel.PageModelBuilder builder = PageModel.builder().found(false);

			Map.Entry<Integer, PageContent> entry = this.getQuery().getPages().entrySet().iterator().next();
			if (entry != null) {
				PageContent content = entry.getValue();
				if (content != null) {
					builder = builder.title(content.getTitle()).pageId(content.getPageId());
					List<RevisionContent> revs = content.getRevisions();
					if (revs != null && revs.size() > 0) {
						RevisionContent rev = content.getRevisions().get(0);
						if (rev != null) {
							if (rev.getMissing() != null) {
								return builder.found(false).build();
							}

							return builder.found(true).content(rev.getContent()).build();
						}
					}
				}
			}

			return builder.found(false).build();
		}
	}

	@Data
	public static class QueryContent {

		private Map<Integer, PageContent> pages;
	}

	@Data
	public static class PageContent {
		@JsonProperty("pageid")
		private int pageId;
		private int ns;
		private String title;
		private List<RevisionContent> revisions;
	}

	@Data
	public static class RevisionContent {
		@JsonProperty("contentformat")
		private String contentFormat;
		@JsonProperty("contentmodel")
		private String contentModel;
		@JsonProperty("*")
		private String content;
		private String missing;
	}
}
